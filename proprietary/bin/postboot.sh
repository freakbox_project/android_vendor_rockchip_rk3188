#!/system/bin/sh

if [ ! -e /data/.postboot ]; then
    busybox touch /data/.postboot
    if [ -e /system/bin/recovery.img ]; then
        flash_image recovery /system/bin/recovery.img
        mount -o rw,remount /dev/block/mtd/by-name/system /system
        rm -f /system/bin/recovery.img
        mount -o ro,remount /dev/block/mtd/by-name/system /system
    fi
fi
exit
